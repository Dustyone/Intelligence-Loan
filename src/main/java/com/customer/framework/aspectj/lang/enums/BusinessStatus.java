package com.customer.framework.aspectj.lang.enums;

/**
 * 操作状态
 * 
 * @author Dustyone
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
