package com.customer.framework.aspectj.lang.enums;

/**
 * 数据源
 * 
 * @author Dustyone
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
