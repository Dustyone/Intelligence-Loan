package com.customer.project.monitor.job.task;

import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 * 
 * @author Dustyone
 */
@Component("cusromerTask")
public class CustomerTask
{
    public void customerParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }

    public void customerNoParams()
    {
        System.out.println("执行无参方法");
    }
}
